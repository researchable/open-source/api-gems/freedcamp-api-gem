#!/usr/bin/env ruby
# typed: false
# frozen_string_literal: true

require 'researchable-freedcamp-api'

# You can add fixtures and/or initialization code here to make experimenting
# with your gem easier. You can also use a different console, if you like.

# (If you use this, don't forget to add pry to your Gemfile!)
# require "pry"
# Pry.start
#
# Researchable::FreedcampApi::ListTasks.run!
ENV['USE_CACHE'] = 'true'
puts Researchable::FreedcampApi::ListTasks.run!(task_id: 44_509_132).to_json

progress = 2
task = Researchable::FreedcampApi::Structs::Task.new(
  project_id: '3033558',
  title: "hoi #{progress}",
  status: progress,
  status_title: 'Ready for review'
)
puts Researchable::FreedcampApi::UpdateTask.run!(task_id: 44_509_132, task: task)

comment = Researchable::FreedcampApi::Structs::Comment.new(
  item_id: '44509132',
  app_id: "2" ,
  description: "this is a test"
)
puts Researchable::FreedcampApi::CreateComment.run!(comment: comment)

# task = Researchable::FreedcampApi::Structs::Task.new(
# project_id: '3033558',
# title: 'hoi',
# task_group_id: '4999322',
# due_date: '2022-01-31',
# assigned_ids: ['1018169']
# )
# Researchable::FreedcampApi::CreateTask.run!(task: task)
