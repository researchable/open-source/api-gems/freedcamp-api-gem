# typed: strict
# frozen_string_literal: true

require 'active_interaction'
require_relative 'freedcamp_api/version'
require_relative 'freedcamp_api/constants'
require_relative 'freedcamp_api/structs'
require_relative 'freedcamp_api/sessions'
require_relative 'freedcamp_api/endpoint'

require_relative 'freedcamp_api/list_projects'
require_relative 'freedcamp_api/list_discussions'


require_relative 'freedcamp_api/list_tasks'
require_relative 'freedcamp_api/update_task'
require_relative 'freedcamp_api/create_task'

require_relative 'freedcamp_api/get_user'

require_relative 'freedcamp_api/list_lists'

require_relative 'freedcamp_api/list_milestones'

require_relative 'freedcamp_api/create_comment'

module Researchable
  module FreedcampApi
    class Error < StandardError; end
    # Your code goes here...
  end
end
