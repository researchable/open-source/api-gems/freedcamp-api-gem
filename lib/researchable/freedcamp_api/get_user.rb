# typed: false
module Researchable
  module FreedcampApi
    class GetUser < Endpoint
      string :user_id, default: 'current'
      # @api private
      def execute
        get_user(user_id)
      end

      #sig do
        #params().returns(
        #)
      #end
      def get_user(task_id)
        session.get("/users/#{task_id}")['data']
      end
    end
  end
end
