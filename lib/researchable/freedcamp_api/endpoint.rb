# typed: strict
#
module Researchable
  module FreedcampApi
    # TODO: Does this work?
    class Endpoint < ActiveInteraction::Base
      # class Endpoint < T::Types::Base
      extend T::Sig
      object :session,
             default: -> { FreedcampApi.signed_session },
             class: Researchable::FreedcampApi::Sessions::Session

      private

      # Fetch all browses paginated responses
      # @param keyword [[TODO:type]] [TODO:description]
      sig { params(keyword: String).returns(T::Array[T.untyped]) }
      def fetch_all(keyword)
        result = T.let([], T::Array[T.untyped])
        has_more = T.let(true, T::Boolean)
        offset = T.let(0, Integer)
        limit = T.let(200, Integer)

        while has_more
          temp = yield limit, offset
          has_more = T.let(temp.dig('data', 'meta', 'has_more') == true, T::Boolean)
          result += temp['data'][keyword]
          offset += limit
        end
        result
      end
    end
  end
end
