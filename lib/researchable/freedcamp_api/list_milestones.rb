# typed: false
module Researchable
  module FreedcampApi
    class ListMilestones < Endpoint
      integer :milestone_id, default: nil
      integer :project_id, default: nil
      integer :f_cf, default: 1

      def execute
        list_milestones(milestone_id)
      end

      # sig do
      # params(
      # task_id: T.nilable(Integer)
      # ).returns(
      # T::Array[Researchable::FreedcampApi::Structs::Task]
      # )
      # end
      def list_milestones(milestone_id)
        milestone_query = milestone_id ? "/#{milestone_id}" : '/'
        project_query = project_id ? "&project_id=#{project_id}" : ''
        custom_fields_query = f_cf.nil? ? '' : "&f_cf=#{f_cf}"
        fetch_all('milestones') do |limit, offset|
          url = "/milestones#{milestone_query}?limit=#{limit}
                  #{project_query}
                  #{custom_fields_query}
                   &offset=#{offset}"
          session.get(url)
        end
      end
    end
  end
end
