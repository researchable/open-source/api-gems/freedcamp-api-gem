# typed: strict
module Researchable
  module FreedcampApi
    module Sessions
      class Session
        extend T::Sig
        extend T::Helpers # (1)
        abstract! # (2)

        sig { abstract.params(path: String).returns(T::Hash[T.untyped, T.untyped]) }
        def get(path); end

        # (3)
        sig do
          abstract.params(
            path: String,
            data: T.any(
              Researchable::FreedcampApi::Structs::Task,
              Researchable::FreedcampApi::Structs::Comment,
              T::Hash[T.untyped, T.untyped]
            )
          ).returns(T.untyped)
        end
        def post(path, data); end
      end
    end
  end
end
