# typed: sigil

module Researchable
  module FreedcampApi
    # @api private
    class CreateComment < Endpoint
      object :comment, class: Researchable::FreedcampApi::Structs::Comment
      def execute
        create_comment(comment)
      end

      private

      sig do
        params(comment: Researchable::FreedcampApi::Structs::Comment)
          .returns(T::Hash[T.untyped, T.untyped])
      end
      def create_comment(comment)
        # title
        session.post('/comments', comment)
      end
    end
  end
end
