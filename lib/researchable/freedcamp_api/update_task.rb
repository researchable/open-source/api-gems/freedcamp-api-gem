# typed: false
#
module Researchable
  module FreedcampApi
    class UpdateTask < Endpoint
      integer :task_id, default: 44_509_132
      object :task, class: Researchable::FreedcampApi::Structs::Task

      def execute
        update_task(task_id, task)
      end

      sig do
        params(
          task_id: Integer,
          task: Researchable::FreedcampApi::Structs::Task
        ).returns(
          T::Array[Researchable::FreedcampApi::Structs::Task]
        )
      end
      def update_task(task_id, task)
        result = session.post("/tasks/#{task_id}", task)
        result['data']['tasks']
      end
    end
  end
end
