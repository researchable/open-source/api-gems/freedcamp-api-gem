# typed: false
# typed: sigil
require 'httparty'
require_relative 'sessions/session'
require_relative 'sessions/signed_session'

module Researchable
  module FreedcampApi
    extend T::Sig

    def self.signed_session(*arguments, &block)
      Researchable::FreedcampApi::Sessions::SignedSession.new(
        *arguments, &block)
#public_key: ENV['PUBLIC_KEY'], secret_key: ENV['SECRET_KEY'], use_cache: true
    end
  end
end
