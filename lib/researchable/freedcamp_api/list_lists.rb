# typed: false
module Researchable
  module FreedcampApi
    class ListLists < Endpoint
      string :project_id, default: nil
      integer :app_id, default: Researchable::FreedcampApi::APP_TODOS
      # @api private
      def execute
        check_app_id(app_id)
        list_lists(app_id, project_id)
      end

      private

      def check_app_id(app_id)
        return if allowed_apps.include? app_id

        raise "app_id not allowed: #{app_id} not in #{allowed_apps}"
      end

      def allowed_apps
        [APP_TODOS, APP_FILES, APP_DISCUSSIONS, APP_WIKI, APP_PASSMAN]
      end

      def list_lists(app_id, project_id)
        project_id_query = project_id ? "&project_id=#{project_id}" : ''
        fetch_all('lists') do |limit, offset|
          url = "/lists/#{app_id}
                  ?limit=#{limit}
                  &offset=#{offset}
                   #{project_id_query}"
          session.get(url)
        end
      end
    end
  end
end
