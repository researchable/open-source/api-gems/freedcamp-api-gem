# typed: strict
# require 'sorbet-runtime'

# (1) Create a new class that subclasses `T::Struct`
module Researchable
  module FreedcampApi
    module Structs
      class Comment < T::Struct
        # Item id to add a comment for
        const :item_id, T.nilable(String)
        # Application id (wiki). Supported apps are
        # - (2) APP_TODOS,
        # - (6) APP_FILES,
        # - 3 APP_DISCUSSIONS,
        # - 13 APP_BUGTRACKER,
        # - 14 APP_WIKI, A
        # - 19 APP_CALENDAR.
        # (see https://freedcamp.com/Mobile_7Yh/iOS_application_6zp/wiki/wiki_public/view/AHaab)
        const :app_id, T.nilable(String)
        # Description of the comment
        const :description, T.nilable(String)
        # Files that might be attached
        const :attached_ids, T.nilable(T::Array[Integer])
      end
    end
  end
end
