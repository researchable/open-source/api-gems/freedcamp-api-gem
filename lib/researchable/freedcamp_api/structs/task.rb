# typed: strict
# require 'sorbet-runtime'

# (1) Create a new class that subclasses `T::Struct`
module Researchable
  module FreedcampApi
    module Structs
      class Task < T::Struct
        const :title, T.nilable(String)
        const :description, T.nilable(String)
        const :project_id, T.nilable(String)
        # Can be empty or absent if task_group_name is passed
        const :task_group_id, T.nilable(String)

        # 	If passed, task_group_id is ignored, and new tasks group with this name will be created
        const :task_group_name, T.nilable(String)

        # 	Will be used if task_group_name passed, can be an empty string or absent
        const :task_group_description, T.nilable(String)

        # 	Priority id. See priorities for :get.
        const :priority, T.nilable(Integer)

        #	Should contain assigned to user id OR one of the two constants:  -1 (ASSIGNED_EVERYONE) and 0 (ASSIGNED_NOONE)
        const :assigned_to_id, T.nilable(String)
        const :assigned_ids, T.nilable(T::Array[String])

        # 	Can be empty or absent, the format is YYYY-MM-DD (empty value will be used if start_date is not allowed for the plan. Old start_date will be overridden in this case, if any).
        const :start_date, T.nilable(String)

        #	The recurrence rule in iCalendar (RFC 5545) format, can be an empty string or absent
        const :r_rule, T.nilable(String)

        # 	Can be empty or absent, the format is YYYY-MM-DD
        const :due_date, T.nilable(String)

        # 	An array of files uploaded previously with temporary=1 flag
        const :attached_ids, T.nilable(Integer)

        # 	Can be absent or empty. It contains a parent task id. Note: parent task should be in the same tasks group as passed task_group_id. WARN! if a project, where subtask is added to, does not allow advanced subtasks (i.e. has f_subtasks_adv = false, see above), added subtask should contain empty string for description and due_date fields, and "0" for priority and assigned_to_id fields. Also, the parent task should not be a subtask. In case when other values are passed, they will be implicitly fixed to the right values with triggering an error sent to Freedcamp developers.
        const :item_parent_id, T.nilable(Integer)

        const :status, T.nilable(Integer)
        const :status_title, T.nilable(String)

        # Shows the top parent task id (so is equal to h_parent_id for subtasks with h_level=1, and differs for subtasks with h_level > 1). Is an empty string for top-level tasks.
        const :h_top_id, T.nilable(String)
      end
    end
  end
end
