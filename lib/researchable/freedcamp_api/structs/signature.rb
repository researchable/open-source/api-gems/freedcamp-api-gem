# typed: strict
#require 'sorbet-runtime'

# (1) Create a new class that subclasses `T::Struct`
class Signature < T::Struct
  # (2) Declare fields on the struct with the `prop` and `const` DSL
  prop :timestamp, Integer
  const :api_key, String
  const :calculated_hash, String
end

