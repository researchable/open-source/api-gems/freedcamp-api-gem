# typed: false
module Researchable
  module FreedcampApi
    class ListDiscussions < Endpoint
      integer :discussion_id, default: nil
      integer :project_id, default: nil

      def execute
        list_discussions(discussion_id)
      end

      # sig do
      # params(
      # task_id: T.nilable(Integer)
      # ).returns(
      # T::Array[Researchable::FreedcampApi::Structs::Task]
      # )
      # end
      def list_discussions(discussion_id)
        discussion_query = discussion_id ? "/#{discussion_id}" : '/'
        project_query = project_id ? "?project_id=#{project_id}" : ''
        url = "/discussions#{discussion_query}
                  #{project_query}"
        session.get(url)
      end
    end
  end
end
