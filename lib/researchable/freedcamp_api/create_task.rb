# typed: false
# typed: sigil
#
module Researchable
  module FreedcampApi
    # @api private
    class CreateTask < Endpoint
      object :task, class: Researchable::FreedcampApi::Structs::Task
      def execute
        create_task(task)
      end

      private

      sig { params(task: Researchable::FreedcampApi::Structs::Task).returns(T::Hash[T.untyped, T.untyped]) }
      def create_task(task)
        # title
        session.post('/tasks', task)
      end
    end
  end
end
