# typed: false
module Researchable
  module FreedcampApi
    class ListProjects < Endpoint
      integer :project_id, default: nil
      integer :f_with_archived, default: 1

      # Accepts "active", "archived", "all" values. Omitting is equal to "active".
      string :lists_status, default: 'active'
      integer :f_cf, default: 1

      # @api private
      def execute
        list_projects(project_id)
      end

      # sig do
      # params(
      # project_id: T.nilable(Integer)
      # ).returns(
      # T::Array[Researchable::FreedcampApi::Structs::Task]
      # )
      # end
      def list_projects(project_id)
        project_query = project_id ? "/#{project_id}" : '/'
        fetch_all('projects') do |limit, offset|
          session.get("/projects#{project_query}?limit=#{limit}
                   &offset=#{offset}")
        end
      end
    end
  end
end
