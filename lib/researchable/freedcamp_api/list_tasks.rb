# typed: false
module Researchable
  module FreedcampApi
    class ListTasks < Endpoint
      integer :task_id, default: nil
      integer :f_with_archived, default: 0
      array :status, default: []

      # Accepts "active", "archived", "all" values. Omitting is equal to "active".
      string :lists_status, default: 'active'
      string :assigned_to_id, default: nil
      integer :f_cf, default: 1
      boolean :include_subtasks, default: true

      # @api private
      def execute
        list_tasks(task_id)
      end

      sig do
        params(
          task_id: T.nilable(Integer)
        ).returns(
          T::Array[Researchable::FreedcampApi::Structs::Task]
        )
      end
      def list_tasks(task_id)
        task_query = task_id ? "/#{task_id}" : '/'
        assigned_to_id_query = assigned_to_id ? "&assigned_to_id[]=#{assigned_to_id}" : ''
        f_with_archived_query = f_with_archived ? "&f_with_archived=#{f_with_archived}" : ''
        status_query = status.reduce('') { |tot, cur| "#{tot}&status[]=#{cur}" }
        # status[]=&status[]=2
        tasks = fetch_all('tasks') do |limit, offset|
          url = "/tasks#{task_query}?limit=#{limit}
                   &offset=#{offset}
                   #{assigned_to_id_query}
                   #{f_with_archived_query}
                   #{status_query}
                   &f_cf=#{f_cf}
                   &lists_status=#{lists_status}"
          session.get(url)
        end

        return tasks if include_subtasks

        tasks.select { |task| task['h_top_id'].blank? }
      end
    end
  end
end
