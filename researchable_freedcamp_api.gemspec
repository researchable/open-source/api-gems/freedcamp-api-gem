# frozen_string_literal: true

require_relative 'lib/researchable/freedcamp_api/version'

Gem::Specification.new do |spec|
  spec.name          = 'researchable-freedcamp-api'
  spec.version       = Researchable::FreedcampApi::VERSION
  spec.authors       = ['Frank Blaauw']
  spec.email         = ['f.j.blaauw@researchable.nl']
  spec.summary       = 'Freedcamp API gem'
  spec.description   = 'Wrapper around the freedcamp api'
  spec.homepage      = 'https://researchable.nl'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/researchable/open-source/api-gems/freedcamp-api-gem'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/researchable/open-source/api-gems/freedcamp-api-gem'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.add_dependency 'active_interaction', '>= 3.2'
  spec.add_dependency 'httparty',           '>= 0.14'
  spec.add_dependency 'sorbet-runtime'

  spec.add_development_dependency 'bundler', '>0'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'sorbet'
end
